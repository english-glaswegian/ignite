import axios from "axios";
// import moment from "moment";

require("dotenv").config();

// Base axios object
export const rawgAxios = axios.create({
  baseURL: `https://api.rawg.io/api/`,
  params: {
    key: process.env.REACT_APP_KEY,
  },
});

// export const rawgDetailAxios = (id) => {
//   return axios.create({
//     baseURL: `https://api.rawg.io/api/`,
//     params: {
//       key: process.env.REACT_APP_KEY,
//     },
//   });
// };
// console.log(process.env.REACT_APP_KEY);

// Popular games
// const popular_games = `games?key=${process.env.REACT_APP_KEY}&dates=${lastYear},${currentDate}&ordering=-rating&page_size=20`;

// export const popularGamesURL = () => `${base_url}${popular_games}`;
// Search axios object
// export const rawgSearch = (game_name) => {
//   return axios.create({
//     baseURL: `https://api.rawg.io/api/games`,
//     params: {
//       key: process.env.REACT_APP_KEY,
//       search: game_name,
//       page_size: 9,
//     },
//   });
// };
//Searched Game
const baseURL = `https://api.rawg.io/api/`;

export const searchGameURL = (game_name) =>
  `${baseURL}games?key=${process.env.REACT_APP_KEY}&search=${game_name}&page_size=9`;
