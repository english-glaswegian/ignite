import { rawgAxios } from "../api";

export const loadDetail = (id, short_screenshots) => async (dispatch) => {
  dispatch({
    type: "LOADING_DETAIL",
  });

  const detailData = await rawgAxios.get(`/games/${id}`);
  const screenShotData = await rawgAxios.get(`/games/${id}/screenshots`);

  dispatch({
    type: "GET_DETAIL",
    payload: {
      game: detailData.data,
      screen: screenShotData,
    },
  });
};
