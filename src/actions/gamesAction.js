// import axios from "axios";
import axios from "axios";
import { rawgAxios, searchGameURL } from "../api";

// Getting the date
const getCurrentMonth = () => {
  const month = new Date().getMonth() + 1;

  if (month < 10) {
    return `0${month}`;
  } else {
    return month;
  }
};

const getCurrentDay = () => {
  const day = new Date().getDay() + 1;

  if (day < 10) {
    return `0${day}`;
  } else {
    return day;
  }
};

// Current day/month/year
const currentYear = new Date().getFullYear();
const currentMonth = getCurrentMonth();
const currentDay = getCurrentDay();
const currentDate = `${currentYear}-${currentMonth}-${currentDay}`;
const lastYear = `${currentYear - 1}-${currentMonth}-${currentDay}`;
const nextYear = `${currentYear + 1}-${currentMonth}-${currentDay}`;

// Action Creator

export const loadGames = () => async (dispatch) => {
  // Fetch axios
  const popularData = await rawgAxios.get("/games", {
    params: {
      dates: `${lastYear},${currentDate}`,
      ordering: "-rating",
      page_size: 20,
    },
  });
  const upcomingData = await rawgAxios.get("/games", {
    params: {
      dates: `${currentDate},${nextYear}`,
      ordering: "-added",
      page_size: 20,
    },
  });
  const newData = await rawgAxios.get("/games", {
    params: {
      dates: `${lastYear},${currentDate}`,
      ordering: "-released",
      page_size: 20,
    },
  });
  dispatch({
    type: "FETCH_GAMES",
    payload: {
      popular: popularData.data.results,
      upcoming: upcomingData.data.results,
      newGames: newData.data.results,
    },
  });
};
export const fetchSearch = (game_name) => async (dispatch) => {
  const searchGames = await axios.get(searchGameURL(game_name));
  dispatch({
    type: "FETCH_SEARCHED",
    payload: {
      searched: searchGames.data.results,
    },
  });
};
